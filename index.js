const got = require('./data.js')
// function 1
function countAllPeople(got) {
  return got.houses.reduce((accumulator, currentArrayElement) => {
    return accumulator + currentArrayElement.people.length
  }, 0)
};

console.log("Total number of people in all houses:", countAllPeople(got))

// function 2
function peopleByHouses(got) {
  let numberOf_peopleInHouse = got.houses.reduce((acc, house) => {
    acc[house.name] = house.people.length
    return acc
  }, {})
  let array = Object.entries(numberOf_peopleInHouse).sort((a, b) => {
    return a[0].localeCompare(b[0])
  })
  return Object.fromEntries(array)
}
console.log(peopleByHouses(got))

// function 3
function everyone(got) {
  return got.houses.reduce((acc, house) => {
    return acc.concat(house.people.map((people) => {
      return people.name
    }))
  }, [])
}

console.log({ peopleNames: everyone(got) })

// function 4
function nameWithS(got) {
  return got.houses.reduce((acc, house) => {
    return acc.concat(house.people.map((people) => {
      return people.name
    }))
  }, []).filter((name) => {
    return name.toUpperCase().includes("S")
  })
}
console.log({ nameWithS: nameWithS(got) })

// function 5
function nameWithA(got) {
  return got.houses.reduce((acc, house) => {
    return acc.concat(house.people.map((people) => {
      return people.name
    }))
  }, []).filter((name) => {
    return name.toUpperCase().includes("A")
  })
}

console.log({ nameWithA: nameWithA(got) })

// function 6
function surnameWithS(got) {
  return got.houses.reduce((acc, house) => {
    let peopleInHouse = house.people.reduce((acc2, people) => {
      if (people.name.split(" ")[1].startsWith("S")) {
        acc2.push(people.name)
      }
      return acc2
    }, [])
    acc = [...acc, ...peopleInHouse]
    return acc
  }, [])
}
console.log({surnamesWithS:surnameWithS(got)})

// function 7
function surnameWithA(got) {
  return got.houses.reduce((acc, house) => {
    let peopleInHouse = house.people.reduce((acc2, people) => {
      if (people.name.split(" ")[1].startsWith("A")) {
        acc2.push(people.name)
      }
      return acc2
    }, [])
    acc = [...acc, ...peopleInHouse]
    return acc
  }, [])
}
console.log({surnamesWithA:surnameWithA(got)})

// function 8
function peopleNameOfAllHouses() {
  let peoplesFromEachHouse = got.houses.reduce((acc, house) => {
    acc[house.name] = house.people.map((person) => person.name)
    return acc
  }, {})
  let entries = Object.entries(peoplesFromEachHouse).sort()
  return Object.fromEntries(entries)
}

console.log({peopleFromEachHouse:peopleNameOfAllHouses()})

// Testing your result after writing your function
// console.log(countAllPeople());
// Output should be 33

// console.log(peopleByHouses());
// Output should be
//{Arryns: 1, Baratheons: 6, Dothrakis: 1, Freys: 1, Greyjoys: 3, Lannisters: 4,Redwyne: 1,Starks: 8,Targaryens: 2,Tullys: 4,Tyrells: 2}

// console.log(everyone());
// Output should be
//["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "King Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy", "Margaery (Tyrell) Baratheon", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn", "Edmure Tully", "Brynden Tully", "Olenna (Redwyne) Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

// console.log(nameWithS(), 'with s');
// Output should be
// ["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "Stannis Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn"]

// console.log(nameWithA());
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon", "Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Yara Greyjoy", "Margaery Baratheon", "Loras Tyrell", "Catelyn Stark", "Lysa Arryn", "Olenna Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

// console.log(surnameWithS(), 'surname with s');
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow", "Catelyn Stark"]

// console.log(surnameWithA());
// Output should be
// ["Lysa Arryn", "Jon Arryn"]

// console.log(peopleNameOfAllHouses());
// Output should be
// {Arryns: ["Jon Arryn"], Baratheons: ["Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon"], Dothrakis: ["Khal Drogo"], Freys: ["Walder Frey"], Greyjoys: ["Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy"], Lannisters: ["Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon"], Redwyne: ["Olenna Tyrell"], Starks: ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow"], Targaryens: ["Daenerys Targaryen", "Viserys Targaryen"], Tullys: ["Catelyn Stark", "Lysa Arryn", "Edmure Tully", "Brynden Tully"], Tyrells: ["Margaery Baratheon", "Loras Tyrell"]}


// Write a function called countAllPeople which counts the total number of people in got variable defined in data.js file.
// Write a function called peopleByHouses which counts the total number of people in different houses in the got variable defined in data.js file.
// Write a function called everyone which returns a array of names of all the people in got variable.
// Write a function called nameWithS which returns a array of names of all the people in got variable whose name includes s or S.
// Write a function called nameWithA which returns a array of names of all the people in got variable whose name includes a or A.
// Write a function called surnameWithS which returns a array of names of all the people in got variable whoes surname is starting with S(capital s).
// Write a function called surnameWithA which returns a array of names of all the people in got variable whoes surname is starting with A(capital a).
// Write a function called peopleNameOfAllHouses which returns an object with the key of the name of house and value will be all the people in the house in an array.